certifi==2024.2.2
charset-normalizer==3.3.2
filelock==3.13.4
fsspec==2024.3.1
huggingface-hub==0.22.2
idna==3.6
joblib==1.4.0
mkl-fft @ file:///croot/mkl_fft_1695058164594/work
mkl-random @ file:///croot/mkl_random_1695059800811/work
mkl-service==2.4.0
numpy @ file:///work/mkl/numpy_and_numpy_base_1682953417311/work
packaging==24.0
pandas==2.0.3
Pillow==9.3.0
protobuf==3.20.0
python-dateutil==2.9.0.post0
pytz==2024.1
PyYAML==6.0.1
regex==2023.12.25
requests==2.31.0
safetensors==0.4.2
scikit-learn==1.3.2
scipy==1.10.1
sentencepiece==0.2.0
six==1.16.0
threadpoolctl==3.4.0
tokenizers==0.13.3
torch==1.7.1
torchaudio==0.7.0a0+a853dff
torchvision==0.8.2
tqdm==4.66.2
transformers==4.28.0
typing_extensions @ file:///croot/typing_extensions_1705599297034/work
tzdata==2024.1
urllib3==2.2.1
