This folder contains the baseline system code files, to reproduce an existing baseline for the task. 
<ul>    
<li>The baseline provided, can be reproduced atleast for a text-only model (config file is defaulted to deBERTa large). But the system can be extended as per the requirement</li>
<li>You need to provide the data paths in "process_data.py"</li>
<li>run "training_script.py" for training.</li>
</ul>