<h2 dir="ltr"><span><strong>Task:</strong> Hero, Villain and Victim: Dissecting harmful memes for Semantic role labelling of entities</span></h2>
<pre>Definition: Given a meme and an entity<span><span>, determine the role* of the entity in the meme: hero vs. villain vs. victim vs. other.<br /></span></span></pre>
<p><em>*The meme is to be analyzed from the perspective of the author of the meme</em></p>
<h4><span style="text-decoration-line: underline; font-size: 1.17em;">Definition of the Entity Classes</span></h4>
<p><strong>Hero</strong></p>
<ul>
<li dir="ltr">
<p dir="ltr"><span>The entity is presented in a </span><span>positive</span><span> light</span></p>
</li>
<li dir="ltr">
<p dir="ltr"><span>Glorified for their actions conveyed via the meme or gathered from background context</span></p>
</li>
</ul>
<p><strong>Villain:</strong></p>
<ul>
<li dir="ltr">
<p dir="ltr"><span>The entity is portrayed </span><span>negatively</span><span>, e.g.,</span><span> in an association with adverse traits like wickedness, cruelty, hypocrisy, etc.&nbsp;</span></p>
</li>
</ul>
<p><strong>Victim:</strong></p>
<ul>
<li dir="ltr">
<p dir="ltr"><span>The entity is portrayed as suffering the negative impact of someone else&rsquo;s actions or conveyed implicitly within the meme.</span></p>
</li>
</ul>
<p><strong>Other:</strong></p>
<ul>
<li dir="ltr">
<p dir="ltr"><span>The entity is not a hero, a villain, or a victim.<br /><br /></span></p>
</li>
</ul>
<h4 dir="ltr"><strong><span style="text-decoration: underline;">Unseen Test Set:</span></strong></h4>
<p dir="ltr"><span>The &ldquo;unseen test set&rdquo; file and the corresponding image set will have samples for three language settings: (a) Bulgarian, (b) English, and (c) En-Hi Code-mixed.</span></p>
<p><span style="text-decoration: underline;">File format:</span></p>
<p><code>{&lsquo;image&rsquo; : memes_1486.png&rsquo;,&lsquo;OCR&rsquo;: "AAE RNC\nCONVENTION 2020:\nHOPEAND\nPOSITIVITY\nDNC CONVENTION 2020:\nDOOM AND GLOOM\n",&lsquo;entity_list&rsquo; :['Donald Trump', 'Joe Biden','Democratic National Convention (DNC)&rsquo;, 'Republican National Convention (RNC)']}</code></p>
<p dir="ltr"><span><code>{&lsquo;image&rsquo; : &lsquo;image_2.png&rsquo;,&lsquo;OCR&rsquo; : "Whole world to\nPutin:\n*Putin\nVaccine\nWell done putin. I'm so proud of you\nVaccine\nVaccine\n",&lsquo;entity_list&rsquo; : [&lsquo;Vladimir Putin&rsquo;, &lsquo;the world&rsquo;, &lsquo;Salman Khan&rsquo;, &lsquo;vaccine&rsquo;]}</code></span></p>
<h4><span><span style="text-decoration: underline;"><strong><span id="docs-internal-guid-f465878b-7fff-a6d8-ed5c-e49b768d1c6b">Submission:</span></strong></span>&nbsp;</span></h4>
<p dir="ltr">Instructions for prediction file submission at Codalab shared-task server (<em>server details below</em>):</p>
<ol>
<li dir="ltr">
<p dir="ltr"><span>The submission NEEDS to be a .zip, with a '.jsonl' predicted results file as its ONLY content.</span></p>
</li>
<li dir="ltr">
<p dir="ltr"><span>Filenames could be anything but should conform to &lt;samplefilename.jsonl&gt;.</span></p>
</li>
<li dir="ltr">
<p dir="ltr"><span>Please DO NOT include anything other than your only '.jsonl' prediction result file.</span></p>
</li>
<li dir="ltr">
<p dir="ltr"><span>Please ensure ALL the test samples (meme image) AND the corresponding entities enlisted in the unseen set, are considered for prediction and appear in your submission file. Please, do NOT leave out any entity unpredicted.</span></p>
</li>
<li dir="ltr"><span style="color: blue; font-weight: bold;">Please be watchful of the submission limits:&nbsp;Max submissions per day: 10; Max submissions per user: 70.</span></li>
<li dir="ltr">
<p dir="ltr"><span>Additionally, please don't forget to email a short text describing your final model details (along with your participation/identification details like username, team name, etc.) to 📧 shiv6891.iitd@gmail.com.</span></p>
</li>
</ol>
<p dir="ltr"><span>For eg. for the samples depicted for the unseen test set, the submission format would look like:</span></p>
<p><span style="text-decoration: underline;">File format:</span></p>
<p dir="ltr"><span><code>{&lsquo;image&rsquo; : memes_1486.png&rsquo;, &lsquo;hero&rsquo; :['Donald Trump'], &lsquo;villain&rsquo; : ['Joe Biden'], &lsquo;victim&rsquo; : [], &lsquo;other&rsquo; : ['Democratic National Convention (DNC)&rsquo;, 'Republican National Convention (RNC)']}</code></span></p>
<p dir="ltr"><span><code>{&lsquo;image&rsquo; : &lsquo;image_2.png&rsquo;, &lsquo;hero&rsquo; : [&lsquo;Vladimir Putin&rsquo;], &lsquo;villain&rsquo; : [], &lsquo;victim&rsquo; : [], &lsquo;other&rsquo; : [&lsquo;the world&rsquo;, &lsquo;Salman Khan&rsquo;, &lsquo;vaccine&rsquo;]}</code></span></p>

<h4><span><span style="text-decoration: underline;"><strong><span id="docs-internal-guid-f465878b-7fff-a6d8-ed5c-e49b768d1c6b">Competetion Site and Submission Portal:</span></strong></span>&nbsp;</span></h4>
The result submission/evaluation is being done separately for the 3 sub-tasks (language settings: En, En-Hi and Bg). The portal links for the respective language settings (sub-tasks) are given below:<br>

<ul>
<li>English (En): <a href="https://codalab.lisn.upsaclay.fr/competitions/18872">CodaLab Link</a></li>
<li>English-Hindi, CodeMixed (En-Hi): <a href="https://codalab.lisn.upsaclay.fr/competitions/18873">CodaLab Link</a></li>
<li>Bulgarian (Bg): <a href="https://codalab.lisn.upsaclay.fr/competitions/18871">CodaLab Link</a></li>
</ul>

<h4><span style="font-size: 1.17em; text-decoration-line: underline;">Evaluation Criteria</span></h4>
<p><span id="docs-internal-guid-294329c5-7fff-0cbe-7f61-097171796866"><span>The official evaluation measure for the shared task is the "</span><span>macro" F1 score</span><span> for the multi-class classification. The evaluation will be done at the corpus level and not at the sample (meme) level.&nbsp;</span></span></p>
<p><span><span>**<strong>Please note</strong>, the evaluation of the submissions will be done on an unseen test set, which is planned to be performed independently for three language settings: (a) Bulgarian, (b) English, and (c) En-Hi Code-mixed.<br /></span></span></p>

<p><span style="text-decoration: underline;"><strong>Instructions for interpreting the results on the competition portal.</strong></span></p>
<ul>
<li>The "correct" label in the leaderboard refers to the Macro F1 score based on which the participants are ranked</li>
<li>To view the Macro Precision, Macro Recall and Macro F1 scores for your submission, use the "+" to expand the specific submission you would like to view for and select "Download output from scoring step".</li>
</ul>
<p><span id="docs-internal-guid-294329c5-7fff-0cbe-7f61-097171796866"><span>&nbsp;</span></span></p>